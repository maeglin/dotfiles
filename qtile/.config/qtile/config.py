# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile.config import Key, Screen, Group, ScratchPad, Drag, Click, Rule, Match, KeyChord
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import Spacer
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
myConfig = "/home/melkor/.config/qtile/config.py"    # The Qtile config file location

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod, "shift"], "Return",
             lazy.spawn("rofi -show run"),
             desc='Dmenu Run Launcher'
             ),
         Key([mod, "control"], "Return",
             lazy.spawn("/home/melkor/scripts/bm.sh"),
             desc='Dmenu Run Launcher'
             ),
         Key([mod, "mod1"], "Return",
             lazy.spawn("/home/melkor/scripts/dmsearch"),
             desc='Dmenu Run Launcher'
             ),
         Key([mod, "mod1"], "n",
             lazy.spawn("nautilus"),
             desc='nautilus'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod], "q",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
        Key([mod], "v",
             lazy.spawn("/opt/resolve/bin/resolve"),
             desc='launch davinci-resolve'
             ),
        Key([mod], "w",
             lazy.spawn("brave-browser-beta --profile-directory='Default'"),
             desc='brave default'
             ),
        ### Switch focus to specific monitor (out of three)
         Key([mod], "r",
             lazy.to_screen(0),
             desc='Keyboard focus to monitor 1'
             ),
         Key([mod], "e",
             lazy.to_screen(1),
             desc='Keyboard focus to monitor 2'
             ),
         Key([mod], "t",
             lazy.to_screen(2),
             desc='Keyboard focus to monitor 3'
             ),
         ### Switch focus of monitors
         Key([mod], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([mod], "comma",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         ### Treetab controls
         Key([mod, "control"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "control"], "j",
             lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
        # Key([mod], "n",
        #     lazy.layout.normalize(),
        #     desc='normalize window size ratios'
        #     ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "f",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
         Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "control"], "m",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         ### Dmenu scripts launched with ALT + CTRL + KEY
         Key([mod, "mod1"], "w",
             lazy.spawn("/home/melkor/builds/Ghostery/Ghostery-bin --args -private-window"),
             desc='private firefox'
             ),
         Key(["mod1", "control"], "m",
             lazy.spawn("./.dmenu/dmenu-sysmon.sh"),
             desc='Dmenu system monitor script'
             ),
         Key(["mod1", "control"], "p",
             lazy.spawn("passmenu"),
             desc='Passmenu'
             ),
         Key(["mod1", "control"], "r",
             lazy.spawn("./.dmenu/dmenu-reddio.sh"),
             desc='Dmenu reddio script'
             ),
         Key(["mod1", "control"], "s",
             lazy.spawn("./.dmenu/dmenu-surfraw.sh"),
             desc='Dmenu surfraw script'
             ),
         Key(["mod1", "control"], "t",
             lazy.spawn("./.dmenu/dmenu-trading.sh"),
             desc='Dmenu trading programs script'
             ),
         Key(["mod1", "control"], "i",
             lazy.spawn("./.dmenu/dmenu-scrot.sh"),
             desc='Dmenu scrot script'
             ),
         ### My applications launched with SUPER + ALT + KEY
         Key([mod, "mod1"], "u",
             lazy.spawn("alacritty -e pulsemixer"),
             desc='pulsemixer'
             ),
         Key([mod, "mod1"], "x",
             lazy.spawn("alacritty -e sudo shutdown -h now"),
             desc='shutdown computer'
             ),
         #KeyChord
         KeyChord([mod], "c", [
             Key([], "v",
                 lazy.spawn("/home/melkor/Applications/Plexamp-3.5.0.AppImage"),
                 desc='open Plex'
                 ),
             Key([], "b",
                 lazy.spawn("/home/melkor/Applications/CPod-1.28.0-x86_64.AppImage"),
                 desc='open Cpod'
                 )
             ]),
         KeyChord([mod], "s", [
             Key([], "a",
                 lazy.spawn("flameshot full -p /home/melkor/Pictures/screenshots"),
                 desc='all screens screenshots'
                 ),
             Key([], "s",
                 lazy.spawn("flameshot screen -p /home/melkor/Pictures/screenshots"),
                 desc='activ screen screenshot'
                 )
             ]),
         KeyChord([mod], "b", [
             Key([], "a",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 2'"),
                 desc='open brave audible profile'
                 ),
             Key([], "m",
                 lazy.spawn("/opt/brave.com/brave-beta/brave-browser-beta '--profile-directory=Profile 1' --app-id=agimnkijcaahngcdmfeangaknmldooml"),
                 desc='open brave youtube-maeglin profile'
                 ),
             Key([], "r",
                 lazy.spawn("/opt/brave.com/brave-beta/brave-browser-beta '--profile-directory=Profile 4' --app-id=agimnkijcaahngcdmfeangaknmldooml"),
                 desc='open brave youtube-rivendel profile'
                 ),
             Key([], "o",
                 lazy.spawn("/opt/brave.com/brave-beta/brave-browser-beta '--profile-directory=Profile 14' --app-id=hfhflbfbdbfkgkjgjjioheljacjjlcff"),
                 desc='open brave youtube-elendil profile'
                 ),
             Key([], "e",
                 lazy.spawn("/opt/brave.com/brave-beta/brave-browser-beta '--profile-directory=Profile 5' --app-id=agimnkijcaahngcdmfeangaknmldooml"),
                 desc='open brave youtube-elendil profile'
                 ),
             Key([], "p",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 6'"),
                 desc='open brave protonmail profile'
                 ),
             Key([], "s",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 7'"),
                 desc='open brave steam profile'
                 ),
             Key([], "c",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 8'"),
                 desc='open brave computer profile'
                 ),
             Key([], "h",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 11'"),
                 desc='open brave binance profile'
                 ),
             Key([], "f",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 3'"),
                 desc='open brave facebook profile'
                 ),
             Key([], "y",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 15'"),
                 desc='open brave Notion profile'
                 ),
             Key([], "t",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 12'"),
                 desc='open brave TV profile'
                 ),
             Key([], "n",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 13'"),
                 desc='open brave Nebula profile'
                 ),
             Key([], "d",
                 lazy.spawn("brave-browser-beta --profile-directory='Profile 10'"),
                 desc='open brave doriath profile'
                 )
             ]),

         KeyChord([mod], "p", [
             Key([], "j",
                 lazy.spawn("/home/melkor/scripts/bm.sh"),
                 desc='open bookmarks'
                 ),
             Key([], "l",
                 lazy.spawn("/home/melkor/scripts/bm-doriath.sh"),
                 desc='open bookmarks'
                 ),
             Key([], "k",
                 lazy.spawn("/home/melkor/scripts/dmsearch"),
                 desc='open websearch menu'
                 )
             ])
]

group_names = [("1", {'layout': 'monadtall'}),
               ("2", {'layout': 'monadtall','matches':[Match(wm_instance_class=['crx_agimnkijcaahngcdmfeangaknmldooml','crx_hfhflbfbdbfkgkjgjjioheljacjjlcff'])]}),
               ("3", {'layout': 'max','matches':[Match(wm_class=['Com.gitlab.newsflash','discord','fluent-reader','Ferdi','Giara','thedesk', 'TelegramDesktop', 'Skype','Caprine','Element','Com.github.geigi.cozy', 'Deezer', 'Pulseeffects', 'IVPN', 'Signal', 'Spotify', 'plexmediaplayer', 'Cawbird'])]}),
               ("4", {'layout': 'monadtall'}),
               ("5", {'layout': 'monadtall','matches':[Match(wm_class=['Microsoft-edge-dev'])]}),
               ("6", {'layout': 'monadtall','matches':[Match(wm_class=['Virt-manager'])]}),
               ("7", {'layout': 'monadtall','matches':[Match(wm_class=['firefox'])]}),
               ("8", {'layout': 'monadtall','matches':[Match(wm_class=['Kodi'])]}),
               ("9", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "#ff5555",
                "border_normal": "1D2330"
                }

layouts = [
    layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
   # layout.Tile(shift_windows=True, **layout_theme),
   layout.Zoomy(columnwidth=600, **layout_theme),
   # layout.Stack(num_stacks=2),
   # layout.TreeTab(
   #      font = "Ubuntu",
   #      fontsize = 10,
   #      sections = ["FIRST", "SECOND"],
   #      section_fontsize = 11,
   #      bg_color = "141414",
   #      active_bg = "90C435",
   #      active_fg = "000000",
   #      inactive_bg = "384323",
   #      inactive_fg = "a0a0a0",
   #      padding_y = 5,
   #      section_top = 10,
   #      panel_width = 320
   #      ),
   # layout.Floating(**layout_theme)
]

#colors = [["#292d3e", "#292d3e"], # panel background
#          ["#434758", "#434758"], # background for current screen tab
#          ["#ffffff", "#ffffff"], # font color for group names
#          ["#ff5555", "#ff5555"], # border line color for current tab
#          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
#          ["#668bd7", "#668bd7"], # color for the even widgets
#          ["#e1acff", "#e1acff"]] # window name

colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#292d3e", "#292d3e"], # border line color for other tab and odd widgets
          ["#292d3e", "#292d3e"], # color for the even widgets
          ["#ff5555", "#ff5555"]] # window name
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

###### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Mononoki Nerdfont",
    fontsize = 16,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Image(
                       filename = "~/.config/qtile/icons/darklogo2.png",
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('dmenu_run')}
                       ),
              widget.GroupBox(
                       font = "Mononoki Nerdfont",
                       fontsize = 14,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Mono",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[0],
             #          foreground = colors[4],
             #          padding = 0,
             #          fontsize = 37
             #          ),
             # widget.TextBox(
             #          text = " ₿",
             #          padding = 0,
             #          foreground = colors[2],
             #          background = colors[4],
             #          fontsize = 12
             #          ),
             # widget.BitcoinTicker(
             #          foreground = colors[2],
             #          background = colors[4],
             #          padding = 5
             #          ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[4],
             #          foreground = colors[5],
             #          padding = 0,
             #          fontsize = 37
             #          ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[0],
                       background = colors[4],
                       padding = 0,
                       scale = 0.7
                       ),
              widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[4],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       padding = 0,
                       fontsize = 37
                       ),


             widget.TextBox(
                       text = " 🌡",
                       padding = 2,
                       foreground = colors[2],
                       background = colors[5],
                       fontsize = 11
                       ),
              widget.ThermalSensor(
                      tag_sensor = "Tdie",
                       foreground = colors[2],
                       background = colors[5],
                       threshold = 90,
                       padding = 5
                       ),
              widget.TextBox(
                       text='',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 37
                       ),
             # widget.TextBox(
             #          text = " ⟳",
             #          padding = 2,
             #          foreground = colors[2],
             #          background = colors[4],
             #          fontsize = 14
             #          ),
             # widget.CheckUpdates(
             #          update_interval = 1800,
             #          distro = 'Arch',
             #          foreground = colors[2],
             #          mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
             #          background = colors[4]
             #          ),
             # widget.TextBox(
             #          text = "Updates",
             #          padding = 5,
             #          mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
             #          foreground = colors[2],
             #          background = colors[4]
             #          ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[4],
             #          foreground = colors[5],
             #          padding = 0,
             #          fontsize = 37
             #          ),
              widget.TextBox(
                       text = " 🖬",
                       foreground = colors[2],
                       background = colors[5],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.Memory(
                       foreground = colors[2],
                       background = colors[5],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 5
                       ),
              widget.TextBox(
                       text='',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 37
                       ),
             # widget.Net(
             #          interface = "enp6s0",
             #          format = '{down} ↓↑ {up}',
             #          foreground = colors[2],
             #          background = colors[4],
             #          padding = 5
             #          ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[4],
             #          foreground = colors[5],
             #          padding = 0,
             #          fontsize = 37
             #          ),
             # widget.TextBox(
             #         text = " Vol:",
             #          foreground = colors[2],
             #          background = colors[5],
             #          padding = 0
             #          ),
             # widget.Volume(
             #          foreground = colors[2],
             #          background = colors[5],
             #          padding = 5
             #          ),
             # widget.TextBox(
             #          text = '',
             #          background = colors[5],
             #          foreground = colors[4],
             #          padding = 0,
             #          fontsize = 37
             #          ),
              widget.Clock(
                       foreground = colors[2],
                       background = colors[5],
                       format = "%A, %B %d  [ %H:%M ]"
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[0],
                       background = colors[5]
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
              ]
    return widgets_list

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2[1:20]                      # Slicing removes unwanted widgets on Monitors 1,3

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.7, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=0.7, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.7, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List


## ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
##BEGIN
#
#########################################################
################ assgin apps to groups ##################
#########################################################
#@hook.subscribe.client_new
# def assign_app_group(client):
#    d = {}
#     #####################################################################################
#     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
#     #####################################################################################
#     d[group_names[0]] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser", "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d[group_names[1]] = [ "Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord", "atom", "subl3", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#     d[group_names[2]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh", "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
#     d[group_names[3]] = ["Gimp", "gimp" ]
#     d[group_names[4]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d[group_names[5]] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d[group_names[6]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer", "virtualbox manager", "virtualbox machine", "vmplayer", ]
#     d[group_names[7]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt", "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d[group_names[8]] = ["Evolution", "Geary", "Mail", "Thunderbird", "evolution", "geary", "mail", "thunderbird" ]
#     d[group_names[9]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious", "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ######################################################################################
#
# wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen(toggle=False)
#
##END
##ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME




main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "true"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart2.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
