#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#Set your native resolution IF it does not exist in xrandr
#More info in the script
#run $HOME/.xmonad/scripts/set-screen-resolution-in-virtualbox.sh

#Find out your monitor name with xrandr or arandr (save and you get this line)
#xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
#xrandr --output DP2 --primary --mode 1920x1080 --rate 60.00 --output LVDS1 --off &
#xrandr --output DisplayPort-0 --primary --mode 2560x1440 --pos 1920x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 --pos 0x158 --rotate normal --output HDMI-A-0 --off --output HDMI-A-1 --off --output DVI-D-0 --mode 1920x1080 --pos 4480x360 --rotate normal

#xrandr --output DisplayPort-0 --mode 3840x2160 --pos 0x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 --pos 6400x0 --rotate left --output HDMI-A-0 --primary --mode 2560x1440 --pos 3840x0 --rotate normal --output HDMI-A-1 --off --output DVI-D-0 --off

xrandr --output DisplayPort-0 --mode 3840x2160 --pos 0x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 --pos 6400x588 --rotate normal --output HDMI-A-0 --primary --mode 2560x1440 --pos 3840x0 --rotate normal --output HDMI-A-1 --off --output DVI-D-0 --off

#xrandr --output DisplayPort-0 --off --output DisplayPort-1 --primary --mode 3840x2160 --pos 2560x0 --rotate normal --output HDMI-A-0 --off --output HDMI-A-1 --mode 2560x1440 --pos 0x0 --rotate normal --output DVI-D-0 --off


#change your keyboard if you need it
#setxkbmap -layout be

#cursor active at boot
#xsetroot -cursor_name left_ptr &

#turn off screen saver
xset s off &
xset -dpms &

#starting utility applications at boot time
aw-qt &
nitrogen --restore &
run nm-applet &
run pamac-tray &
run telegram-desktop &
run spotify &
run cawbird &
run plexmediaplayer &
run pulseeffects &
run volumeicon &
run virt-manager &
run picom  --experimental-backends --config $HOME/.config/picom/picom.conf &
run corectrl &
run com.gitlab.newsflash &
run discord &
run flameshot &
run microsoft-edge-dev &
run skype &
run telegram-desktop &
#run brave-browser-beta --profile-directory='Profile 3'&
#run blueman-applet &
#run brave-browser-beta --profile-directory='Profile 6'&
#run /opt/brave.com/brave-beta/brave-browser-beta "--profile-directory=Profile 1" --app-id=agimnkijcaahngcdmfeangaknmldooml &
#run /opt/brave.com/brave-beta/brave-browser-beta "--profile-directory=Profile 4" --app-id=agimnkijcaahngcdmfeangaknmldooml &
#run /opt/brave.com/brave-beta/brave-browser-beta "--profile-directory=Profile 5" --app-id=agimnkijcaahngcdmfeangaknmldooml &
#caprine &
#signal-desktop &
#com.github.geigi.cozy &
#starting user applications at boot time
#run firefox &
